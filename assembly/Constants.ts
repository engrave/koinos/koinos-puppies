import { Base58 } from "@koinos/sdk-as";

export namespace Constants {
  export const NAME: string = "Koinos Puppies";
  export const SYMBOL: string = "KOPUP";
  export const MINT_PRICE: u64 = 5500000000;
  export const MINT_FEE: bool = true;
  export const MAX_SUPPLY: u64 = 1000;
  export const URI: string = "https://koinospuppies.mypinata.cloud/ipfs/QmUy78C83JPsxVnmLnP1AoMAX89qyEw11Ftt1dDKVjBiBr/";
  export const OWNER: Uint8Array = Base58.decode("1A1kF4RvfBYg5Pkp7Ee8mbAL7pY9HnNfc3");

  // token mint
  export const TOKEN_PAY: Uint8Array = Base58.decode("15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL"); // Koin
  export const ADDRESS_PAY: Uint8Array = Base58.decode("1A1kF4RvfBYg5Pkp7Ee8mbAL7pY9HnNfc3");
}